/*
  Warnings:

  - You are about to drop the column `cdr` on the `Megis` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Megis" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT NOT NULL,
    "megis" INTEGER NOT NULL,
    "cdrActive" BOOLEAN,
    "megisCd" TEXT,
    "cdrCd" TEXT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    CONSTRAINT "Megis_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_Megis" ("createdAt", "id", "megis", "updatedAt", "userId") SELECT "createdAt", "id", "megis", "updatedAt", "userId" FROM "Megis";
DROP TABLE "Megis";
ALTER TABLE "new_Megis" RENAME TO "Megis";
CREATE UNIQUE INDEX "Megis_userId_key" ON "Megis"("userId");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;

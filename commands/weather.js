const command = {
  name: "weather",
  desc: "get the weather from wttr.in",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (splitted[2] === undefined) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, supply a location PoroSad`
      );

      return;
    }

    const data = await fetch(
      `https://wttr.in/${encodeURIComponent(splitted[2])}?format=j1`
    )
      .then((res) => res.json())
      .then((data) => {
        return data.current_condition[0];
      })
      .catch((err) =>
        console.log("Error fetching from wttr.in: " + err.message)
      );

    if (data === undefined) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, Invalid location PoroSad`
      );

      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName} Weather for ${splitted[2]} Status: ${data.weatherDesc[0]?.value} Temp: ${data.temp_C}°C Feels like: ${data.FeelsLikeC}°C`
    );
  },
};

module.exports = { command };

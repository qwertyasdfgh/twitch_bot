const command = {
  name: "google",
  desc: "google search",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const data = await fetch(
      `https://search.femboy.hu/api.php?q="${encodeURIComponent(
        splitted.slice(2).join(" ")
      )}"`
    )
      .then((res) => res.json())
      .then((data) => data[0])
      .catch((err) =>
        console.error("Error when fetching results: " + err.message)
      );

    if (!data) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, no results found or api is down`
      );

      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, First result for query ${splitted
        .slice(2)
        .join(" ")}: ${
        "special_response" in data
          ? `${data.special_response.response.replaceAll("\n", "")} - ${
              data.special_response.source
            }`
          : `${data.title} - ${data.description} ${data.url}`
      }`
    );
  },
};

module.exports = { command };

const command = {
  name: "massping",
  desc: "ping every chatter in the room (this one also blocks the thread until it's done)",
  restricted: true,
  mod: true,
  run: async (client, msg, splitted) => {
    let channel = msg.channelName;
    if (splitted[2]) channel = splitted[2];

    const data = await fetch(
      `https://tmi.twitch.tv/group/user/${encodeURIComponent(channel)}/chatters`
    )
      .then((res) => res.json())
      .then((data) => {
        return data.chatters;
      })
      .catch((err) =>
        console.error("Error when fetching chatters: " + err.message)
      );

    let names = [];
    function printValues(obj) {
      for (var key in obj) {
        if (typeof obj[key] === "object") {
          printValues(obj[key]);
        } else {
          names.push(obj[key]);
        }
      }
    }

    printValues(data);
    let temp = [];
    for (let name of names) {
      if (temp.join(" ").length > 450) {
        client.say(msg.channelName, temp.join(" "));
        temp = [];
      }

      temp.push(name);
      if (names.slice(-1)[0] === name) {
        client.say(msg.channelName, temp.join(" "));
      }
    }
  },
};

module.exports = { command };

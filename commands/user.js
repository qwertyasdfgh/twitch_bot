const command = {
  name: "user",
  desc: "get info about a certain user",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const data = await fetch(
      `https://api.ivr.fi/v2/twitch/user?login=${
        splitted[2]
          ? encodeURIComponent(splitted[2])
          : encodeURIComponent(msg.displayName)
      }`
    )
      .then((res) => res.json())
      .then((data) => data[0])
      .catch((err) => console.error("Error with leppunen api: " + err.message));

    await client.say(
      msg.channelName,
      `${msg.displayName}, information about user ${
        data.displayName
      }: Created at: ${new Date(data.createdAt).toLocaleString()}, ${
        data.banned ? `Banned: yes; Reason: ${data.banReason}` : ""
      } ${
        data.lastBroadcast.startedAt
          ? `Last stream: ${data.lastBroadcast.title}; ${Math.floor(
              (new Date().getTime() -
                new Date(data.lastBroadcast.startedAt).getTime()) /
                (1000 * 3600 * 24)
            )} days ago`
          : ""
      }`
    );
  },
};

module.exports = { command };

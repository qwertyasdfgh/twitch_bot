const prisma = require("../clients/prisma");
const megisTypes = {
  0: "bcaa megis PoroSad",
  7: "lemon sugar free megis BroBalt",
  15: "sugar free megis! PogChamp",
  30: "normal megis!!! AngelThump",
};

function secondsToHMS(secs) {
  function z(n) {
    return (n < 10 ? "0" : "") + n;
  }
  var sign = secs < 0 ? "-" : "";
  secs = Math.abs(secs);
  return (
    sign +
    z((secs / 3600) | 0) +
    ":" +
    z(((secs % 3600) / 60) | 0) +
    ":" +
    Math.floor(z(secs % 60))
  );
}

// this command does things that should be done seperately,
// probably will extract adding the user to the database to
// another command when i start doing another command that also needs that functionality
const command = {
  name: "megis",
  desc: "economy command: get megis once per hour",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const now = new Date().getTime();
    const user = await prisma.user.findUnique({
      where: {
        userId: msg.senderUserID,
      },
      include: {
        megis: true,
      },
    });

    if (!user) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, run '% megis start' first`
      );
      return;
    }

    if (user && msg.senderUsername !== user.name) {
      const changeName = await prisma.user.update({
        where: {
          userId: msg.senderUserID,
        },
        data: {
          name: msg.senderUsername,
        },
      });

      if (!changeName) {
        throw new Error(
          `Tried to change username from ${user.name} to ${msg.senderUsername} but failed`
        );
      }
    }

    switch (splitted[2]) {
      case "start": {
        if (user) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, already started`
          );
          return;
        }

        const createUser = await prisma.user.create({
          data: {
            userId: msg.senderUserID,
            name: msg.senderUsername,
            megis: {
              create: {
                megis: 0,
                cdrActive: false,
              },
            },
          },
          include: {
            megis: true,
          },
        });

        if (!createUser) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, something went wrong with adding you to the database`
          );

          return;
        }

        await client.say(msg.channelName, `${msg.displayName}, success`);
        break;
      }

      case "cdr": {
        if (user.megis.cdrActive) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, you already have a cdr active`
          );

          return;
        }
        if (now < user.megis.cdrCd) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, cdr still on cooldown for ${secondsToHMS(
              (user.megis.cdrCd - now) / 1000
            )} 🕒`
          );

          return;
        }
        if (user.megis.megis < 15) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, you have don't have enough megis to buy a cdr! You have ${user.megis.megis} `
          );

          return;
        }

        const cdr = await prisma.user.update({
          where: {
            userId: msg.senderUserID,
          },
          data: {
            megis: {
              update: {
                megis: {
                  decrement: 15,
                },
                cdrActive: true,
                cdrCd: String(now + 1800000),
              },
            },
          },
        });

        if (!cdr) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, something went wrong with resetting your cdr`
          );

          return;
        }

        await client.say(
          msg.channelName,
          `${msg.displayName}, no sleep tonight... (-15 megis: cooldown reset)`
        );

        break;
      }

      case "lb": {
        const allMegisers = await prisma.user.findMany({
          take: 3,
          orderBy: {
            megis: {
              megis: "desc",
            },
          },
          include: {
            megis: true,
          },
        });

        await client.say(
          msg.channelName,
          `${msg.displayName}, top megisers! ${allMegisers
            .map(
              (item, index) =>
                `${index + 1}. ${item.name}: ${item.megis.megis} megis`
            )
            .join(", ")}`
        );

        break;
      }

      default: {
        if (!user.megis.cdrActive) {
          const timeMs = now;
          if (timeMs < user.megis.megisCd) {
            await client.say(
              msg.channelName,
              `${
                msg.displayName
              }, Two cans per day... (on timeout for ${secondsToHMS(
                (user.megis.megisCd - now) / 1000
              )} 🕒 ) `
            );
            return;
          }
        }
        const megis =
          Object.keys(megisTypes)[
            Math.floor(Math.random() * Object.keys(megisTypes).length)
          ];
        const updateUser = await prisma.user.update({
          where: {
            userId: msg.senderUserID,
          },
          data: {
            megis: {
              update: {
                megis: {
                  increment: parseInt(megis),
                },
                megisCd: String(now + 900000),
                cdrActive: false,
              },
            },
          },
          include: {
            megis: true,
          },
        });

        if (!updateUser) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, something went wrong`
          );

          return;
        }

        await client.say(
          msg.channelName,
          `${msg.displayName}, +${megis} megis! ${megisTypes[megis]} Total megis: ${updateUser.megis.megis}`
        );

        break;
      }
    }
  },
};

module.exports = { command };

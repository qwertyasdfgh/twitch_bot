const prisma = require("../clients/prisma");

const command = {
  name: "join",
  desc: "make the bot listen to a channel",
  restricted: false,
  mod: false,
  run: async (client, msg) => {
    if (msg.channelName !== process.env.TWITCH_USERNAME) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, command only in available in bots chat`
      );

      return;
    }
    if (client.joinedChannels.has(msg.senderUsername)) {
      await client.say(msg.channelName, `${msg.displayName}, already joined`);

      return;
    }

    console.log("Joining channel: " + msg.senderUsername);
    const storeChannel = await prisma.channel.create({
      data: {
        name: msg.senderUsername,
      },
    });

    if (!storeChannel) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, something went wrong saving`
      );

      return;
    }

    await client
      .join(msg.senderUsername)
      .then(
        async () =>
          await client.say(
            msg.channelName,
            `${msg.displayName}, joined ${msg.senderUsername} BroBalt`
          )
      );

    await client
      .say(msg.senderUsername, "MrDestructoid")
      .catch((err) =>
        console.error(
          `The bot can't speak in the newly joined channel! Error: ${err.message}`
        )
      );
  },
};

module.exports = { command };

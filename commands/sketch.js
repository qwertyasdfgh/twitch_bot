const command = {
  name: "sketch",
  desc: "make a suspicious link",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (!splitted[2]) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, please supply an URL PoroSad`
      );
      return;
    }

    let formData = new FormData();
    formData.append(
      "long_url",
      splitted[2].startsWith("https://") || splitted[2].startsWith("http://")
        ? splitted[2]
        : `https://${splitted[2]}`
    );

    const data = await fetch(
      `https://verylegit.link/sketchify?long_url=twitter.com`,
      {
        body: formData,
        method: "POST",
      }
    )
      .then((res) => res.text())
      .catch((err) => console.error("Error sketchifying url: " + err.message));

    await client.say(
      msg.channelName,
      `${msg.displayName}, sketchified URL: ${data}`
    );
  },
};

module.exports = { command };

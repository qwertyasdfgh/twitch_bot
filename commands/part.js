const prisma = require("../clients/prisma");

const command = {
  name: "part",
  desc: "part the joined channel",
  restricted: false,
  mod: false,
  run: async (client, msg) => {
    if (msg.channelName !== process.env.TWITCH_USERNAME) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, command only available in bot chat`
      );
      return;
    }

    if (!client.joinedChannels.has(msg.senderUsername)) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, not currently joined there`
      );
      return;
    }

    console.log("Parting channel: " + msg.senderUsername);
    const partChannel = await prisma.channel.delete({
      where: {
        name: msg.senderUsername,
      },
    });

    if (!partChannel) {
      console.error("Couldn't part channel: " + msg.senderUsername);
      await client.say(
        msg.channelName,
        `${msg.displayName}, couldn't save parting changes`
      );
      return;
    }

    await client
      .part(msg.senderUsername)
      .then(
        async () =>
          await client.say(msg.channelName, `${msg.displayName}, parted`)
      )
      .catch((err) =>
        console.error(
          "Something went wrong with parting with the channel: " + err.message
        )
      );
  },
};

module.exports = { command };

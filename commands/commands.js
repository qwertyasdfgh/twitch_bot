const command = {
  name: "commands",
  desc: "get list of commands",
  restricted: false,
  mod: false,
  run: async (client, msg) => {
    await client.say(
      msg.channelName,
      `${msg.displayName}, ${
        client.commands.length
      } commands; List of commands: ${client.commands
        .map((cmd) => cmd.name)
        .join(", ")}`
    );
  },
};

module.exports = { command };

const command = {
  name: "quora",
  desc: "get an answer to a question from quora",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const data = await fetch(
      `https://quetre.iket.me/api/v1/search?q=${encodeURIComponent(
        splitted.slice(2).join(" ")
      )}&type=answer`
    )
      .then((res) => res.json())
      .then((data) => data.data.results[0].text)
      .catch((err) => console.error("Error fetching quora: " + err.message));

    if (!data) {
      await client.say(msg.channelName, `${msg.displayName}, nothing found`);

      return;
    }

    let str = "";
    data.map((lol) => lol.spans.map((span) => (str += ` ${span.text}`)));

    await client.say(
      msg.channelName,
      `${msg.displayName}, ${str.replaceAll("\n", "").substring(0, 450)}`
    );
  },
};

module.exports = { command };

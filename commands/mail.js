const command = {
  name: "mail",
  desc: "temporary email using dropmail.me",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    switch (splitted[2]) {
      case "create": {
        const data = await fetch(
          `https://dropmail.me/api/graphql/gj3489g389gj4wegj3qj890fg23j89j82g?query=mutation%20%7BintroduceSession%20%7Bid%2C%20expiresAt%2C%20addresses%20%7Baddress%7D%7D%7D`
        )
          .then((res) => res.json())
          .then((data) => data.data.introduceSession)
          .catch((err) =>
            console.error("Error when creating a mailbox: " + err.message)
          );

        await client.say(
          msg.channelName,
          `${msg.displayName}, your temporary email has been created! Use the ID to fetch new mails! ID: ${data.id} Address: ${data.addresses[0].address}`
        );
        break;
      }

      case "fetch": {
        const data = await fetch(
          `https://dropmail.me/api/graphql/gj3489g389gj4wegj3qj890fg23j89j82g?query=query%20(%24id%3A%20ID!)%20%7Bsession(id%3A%24id)%20%7B%20addresses%20%7Baddress%7D%2C%20mails%7BrawSize%2C%20fromAddr%2C%20toAddr%2C%20downloadUrl%2C%20text%2C%20headerSubject%7D%7D%20%7D&variables=%7B%22id%22%3A%22${encodeURIComponent(
            splitted[3]
          )}%22%7D`
        )
          .then((res) => res.json())
          .then((data) => data.data.session)
          .catch((err) =>
            console.error("Error when fetching mailbox: " + err.message)
          );

        if (data.mails.length === 0) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, no mails found`
          );

          return;
        }

        await client
          .say(
            msg.channelName,
            `${msg.displayName}, newest mail from ${data.addresses[0].address}: Title: ${data.mails[0].headerSubject} `
          )
          .catch((err) => console.log(err));

        await client.say(
          msg.channelName,
          `${msg.displayName}, Body: ${data.mails[0].text
            .replaceAll("\n", " ")
            .substring(0, 400)}`
        );
        break;
      }

      default: {
        await client.say(
          msg.channelName,
          `${msg.displayName}, supply an action (create or fetch)`
        );
        break;
      }
    }
  },
};

module.exports = { command };

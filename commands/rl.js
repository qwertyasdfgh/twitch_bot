const command = {
  name: "rl",
  desc: "get a random line from a channel",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const data = await fetch(
      `https://logs.ivr.fi/channel/${
        splitted[2]
          ? encodeURIComponent(splitted[2])
          : encodeURIComponent(msg.channelName)
      }/user/${
        splitted[3]
          ? encodeURIComponent(splitted[3])
          : encodeURIComponent(msg.displayName)
      }/random?json=json`
    )
      .then((res) => res.json())
      .then((data) => data.messages[0])
      .catch((err) =>
        console.error("Error fetching leppunen api: " + err.message)
      );

    if (!data) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, nothing found or ivr api is down`
      );

      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, [${new Date(data.timestamp).toLocaleString()}] ${
        data.username
      }: ${data.text}`
    );
  },
};

module.exports = { command };

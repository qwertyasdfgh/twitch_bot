const command = {
  name: "rd",
  desc: "get post from reddit subreddit",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const data = await fetch(
      `https://teddit.namazso.eu/r/${
        splitted[2] ? encodeURIComponent(splitted[2]) : "forsen"
      }?api`
    )
      .then((res) => res.json())
      .then((data) => data.links[Math.floor(Math.random() * data.links.length)])
      .catch((err) =>
        console.error("Error fetching from teddit: " + err.message)
      );

    if (!data) {
      client.say(
        msg.channelName,
        `${msg.displayName}, nothing found or api is down`
      );
      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, r/${splitted[2] ? splitted[2] : "forsen"}: ${
        data.title
      } (Score: ${data.score} Ratio: ${data.upvote_ratio}) ${data.url}`
    );
  },
};

module.exports = { command };

const command = {
  name: "translate",
  desc: "translate something",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (!splitted[2] || !splitted[3]) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, needed <target country code> <text to be translated>`
      );
      return;
    }

    const data = await fetch(
      `https://lingva.ml/api/v1/auto/${encodeURIComponent(
        splitted[2]
      )}/${encodeURIComponent(splitted.slice(3).join(" "))}`
    )
      .then((res) => res.json())
      .catch((err) => console.error("Error with translating: " + err.message));

    if ("error" in data) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, invalid country code or text`
      );

      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, Translation of ${splitted
        .slice(3)
        .join(" ")} to lang ${splitted[2]}: '${data.translation}'`
    );
  },
};

module.exports = { command };

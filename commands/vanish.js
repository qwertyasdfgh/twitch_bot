const command = {
  name: "vanish",
  desc: "disapper from sight",
  restricted: false,
  mod: true,
  run: async (client, msg) => {
    await client.timeout(msg.channelName, msg.senderUsername, 1);
  },
};

module.exports = { command };

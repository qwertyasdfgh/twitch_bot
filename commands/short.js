const command = {
  name: "short",
  desc: "shorten an url",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (!splitted[2]) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, supply an URL PoroSad`
      );
      return;
    }

    const formData = new FormData();
    formData.append(
      "url",
      splitted[2].startsWith("https://") || splitted[2].startsWith("http://")
        ? splitted[2]
        : `https://${splitted[2]}`
    );

    const data = await fetch(`https://s.lain.la`, {
      method: "post",
      body: formData,
    })
      .then((res) => res.text())
      .catch((err) => console.log("Error shortening url: " + err.message));

    await client.say(
      msg.channelName,
      `${msg.displayName}, shortened URL: ${data}`
    );
  },
};

module.exports = { command };

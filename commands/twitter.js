const command = {
  name: "twitter",
  desc: "fetch latest tweet from user",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    const data = await fetch(
      `https://api.rss2json.com/v1/api.json?rss_url=https://nitter.namazso.eu/${
        splitted[2] ? encodeURIComponent(splitted[2]) : "forsen"
      }/rss`
    )
      .then((res) => res.json())
      .then((data) => data.items[0])
      .catch((err) => console.error("Error with rss2json: " + err.message));

    if (!data) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, invalid name or api's are down`
      );
      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, Latest tweet for ${
        splitted[2] ? splitted[2] : "forsen"
      }: "${data.title.replaceAll("\n", "")}" Media: ${data.description
        .split('"')
        .filter((item) => item.startsWith("https://"))
        .join(" ")} Link: ${data.link}`
    );
  },
};

module.exports = { command };

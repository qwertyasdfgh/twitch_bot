const command = {
  name: "fill",
  desc: "fill the chat with something",
  restricted: false,
  mod: true,
  run: async (client, msg, splitted) => {
    if (!splitted[2]) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, Supply an arg PoroSad`
      );

      return;
    }

    let str = "";
    while (true) {
      if (str.length + splitted.slice(2).join(" ").length >= 450) break;
      str += `${splitted.slice(2).join(" ")} `;
    }

    await client.me(msg.channelName, str);
  },
};

module.exports = { command };

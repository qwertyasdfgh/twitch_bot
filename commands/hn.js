const command = {
  name: "hn",
  desc: "fetch hacker news",
  restricted: false,
  mod: false,
  run: async (client, msg) => {
    const data = await fetch(
      `https://hn.algolia.com/api/v1/search?tags=front_page`
    )
      .then((res) => res.json())
      .then((data) => data.hits[Math.floor(Math.random() * data.hits.length)])
      .catch((err) =>
        console.error("Error when fetching hacker news: " + err.message)
      );

    await client.say(
      msg.channelName,
      `${msg.displayName}, Hacker News: ${data.title} (Points: ${data.points}, Comments: ${data.num_comments}) Article: ${data.url} Comments: https://news.ycombinator.com/item?id=${data.objectID}`
    );
  },
};

module.exports = { command };

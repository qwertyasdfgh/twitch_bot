const command = {
  name: "pyramid",
  desc: "make a pyramid (this blocks the thread and no other command will execute until it's done)",
  restricted: true,
  mod: true,
  run: async (client, msg, splitted) => {
    if (
      isNaN(splitted[2]) ||
      splitted[2] === undefined ||
      splitted[3] === undefined ||
      parseInt(splitted[2]) > 50
    ) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, Second arg needs to be a valid number and third arg needs to exist PoroSad`
      );

      return;
    }
    let arr = [];
    for (let i = 0; i < parseInt(splitted[2]); i++) {
      arr.push(splitted.slice(3).join(" "));
      client.say(msg.channelName, arr.join(" "));
    }

    for (let i = parseInt(splitted[2]); i > 1; i--) {
      arr.pop();
      client.say(msg.channelName, arr.join(" "));
    }
  },
};

module.exports = { command };

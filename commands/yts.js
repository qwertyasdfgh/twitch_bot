const command = {
  name: "yts",
  desc: "search youtube for a video",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (splitted[2] === undefined) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, supply a search PoroSad`
      );
      return;
    }

    const data = await fetch(
      `https://invidious.epicsite.xyz/api/v1/search?type=video&q=${encodeURIComponent(
        splitted.slice(2).join(" ")
      )}`
    )
      .then((res) => res.json())
      .then((data) => data[0])
      .catch((err) =>
        console.log("Error fetching from invidious: " + err.message)
      );

    if (!data) {
      await client.say(msg.channelName, `${msg.displayName}, nothing found`);
      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, YouTube video with search term: ${splitted
        .slice(2)
        .join(" ")}; ${data.title} by ${data.author}, Link: youtu.be/watch?v=${
        data.videoId
      }`
    );
  },
};

module.exports = { command };

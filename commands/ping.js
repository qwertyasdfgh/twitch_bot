const os = require("os");
const prisma = require("../clients/prisma");

const command = {
  name: "ping",
  desc: "uptime",
  restricted: false,
  mod: false,
  run: async (client, msg) => {
    await client.say(
      msg.channelName,
      `${msg.displayName}, Pong! Uptime: ${Math.floor(
        process.uptime()
      )}s Memory used: ${Math.floor(
        os.totalmem() / 1024 / 1024 - os.freemem() / 1024 / 1024
      )}mb / ${Math.floor(
        os.totalmem() / 1024 / 1024
      )}mb Joined channels count: ${
        client.joinedChannels.size
      } Megis drinkers count: ${await prisma.megis.count()}`
    );
  },
};

module.exports = { command };

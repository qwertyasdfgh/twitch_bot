const command = {
  name: "fm",
  desc: "get last played track from last.fm with your username",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (splitted[2] === undefined) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, supply your Last.FM username PoroSad`
      );
      return;
    }

    const data = await fetch(
      `https://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=${encodeURIComponent(
        splitted[2]
      )}&api_key=${process.env.LASTFM_KEY}&format=json&limit=1`
    )
      .then((res) => res.json())
      .then((data) => {
        return data.recenttracks.track[0];
      })
      .catch((err) =>
        console.error("Error when fetching for last.fm: " + err.message)
      );

    if (data === undefined) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, Invalid user PoroSad`
      );
      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, (Now Playing) Album: ${data.album["#text"]} Track: ${data.name} Artist: ${data.artist["#text"]} | ${data.url}`
    );
  },
};

module.exports = { command };

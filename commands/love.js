const command = {
  name: "love",
  desc: "get the amount of love between two people!",
  restricted: false,
  mod: false,
  run: async (client, msg, splitted) => {
    if (splitted[2] === undefined || splitted[3] === undefined) {
      await client.say(
        msg.channelName,
        `${msg.displayName}, supply 2 args PoroSad`
      );
      return;
    }

    await client.say(
      msg.channelName,
      `${msg.displayName}, ${splitted[2]} and ${
        splitted[3]
      } are about ${Math.floor(Math.random() * 100)}% in love with each other!`
    );
  },
};

module.exports = { command };

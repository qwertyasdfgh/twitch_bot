const {
  AlternateMessageModifier,
  ChatClient,
  SlowModeRateLimiter,
} = require("dank-twitch-irc");
const { getCommands } = require("../utils/commands");
const prisma = require("./prisma");

let client = new ChatClient({
  username: process.env.TWITCH_USERNAME,
  password: process.env.TWITCH_PASSWORD,

  rateLimits: "default",
  ignoreUnhandledPromiseRejections: true,
});

client.use(new SlowModeRateLimiter(client));
client.use(new AlternateMessageModifier(client));
client.commands = getCommands();
console.log("Commands loaded");

client.on("ready", async () => {
  console.log("Connected to chat");

  console.log("Joining bots channel: " + process.env.TWITCH_USERNAME);
  client.join(process.env.TWITCH_USERNAME);

  const channels = await prisma.channel.findMany();
  for (let channel of channels) {
    console.log("Joining channel: " + channel.name);
    client.join(channel.name);
  }

  console.log("Joined channels");
});

client.on("close", async (error) => {
  if (error != null) {
    console.error("Client closed due to error", error);
  }
});

client.on("PRIVMSG", async (msg) => {
  if (msg.senderUsername === process.env.TWITCH_USERNAME) return;
  if (msg.messageText.length < 3) return;
  const splitted = msg.messageText.split(" ");

  // chatterinos duplicate message bypass fix lol
  if (splitted[1] === "") {
    splitted.splice(1, 1);
  }

  if (splitted[0] === process.env.BOT_PREFIX) {
    try {
      if (!(splitted[1] in client.commands)) {
        await client.say(msg.channelName, `${msg.displayName}, not a command`);
        return;
      }

      if (client.commands[splitted[1]].restricted) {
        if (!msg.badges.hasBroadcaster) {
          if (!msg.isMod) {
            await client.say(
              msg.channelName,
              `${msg.displayName}, need to be a mod or brodcaster to use`
            );
            return;
          }
        }
      }

      if (
        !client.userStateTracker.channelStates[msg.channelName].badges
          .hasBroadcaster
      ) {
        if (
          client.commands[splitted[1]].mod &&
          !client.userStateTracker.channelStates[msg.channelName].isMod
        ) {
          await client.say(
            msg.channelName,
            `${msg.displayName}, bot needs to be a mod or brodcaster to use`
          );

          return;
        }
      }

      await client.commands[splitted[1]].run(client, msg, splitted);
    } catch (err) {
      console.error("Something very unexpected happened: " + err.message);
    }
  }
});

client.connect();

module.exports = { client };

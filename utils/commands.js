const fs = require("fs");

function getCommands() {
  const commands = [];
  const commandFiles = fs
    .readdirSync("./commands")
    .map((file) => file.replace(".js", ""));

  for (let file of commandFiles) {
    import(`../commands/${file}.js`)
      .then((cmd) => commands.push((commands[file] = cmd.command)))
      .catch((err) =>
        console.error(
          `Error when trying to read command file ${file}: ${err.message}`
        )
      );
  }

  return commands;
}

module.exports = { getCommands };

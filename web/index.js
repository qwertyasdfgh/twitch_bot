const fastify = require("fastify")({ logger: true });
const { client } = require("../clients/twitch.js");
const path = require("path");

fastify.register(require("@fastify/static"), {
  root: path.join(__dirname, "front/dist"),
  wildcard: false,
});

fastify.get("/api/v1/commands", (req, res) => {
  return client.commands;
});

fastify.get("/api/v1/commands/:name", (req, res) => {
  if (!(req.params.name in client.commands)) {
    return { error: "Command not found" };
  }

  return client.commands[req.params.name];
});

fastify.get("*", (req, res) => {
  return res.sendFile("index.html");
});

const start = async () => {
  try {
    await fastify.listen({ port: 3000 });
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();

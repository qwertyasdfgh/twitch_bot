import { useLoaderData, useNavigation } from "react-router-dom";

export async function loader({ params }) {
  return await fetch(`/api/v1/commands/${params.commandName}`)
    .then((res) => res.json())
    .catch((err) => console.log(err));
}

export function Command() {
  const data = useLoaderData();
  const navigation = useNavigation();
  return (
    <div className="m-4 rounded p-4 shadow">
      {navigation.state === "loading" ? (
        <p className="absolute text-gray-400">Loading command...</p>
      ) : (
        ""
      )}

      {data.error ? (
        <div className="rounded-lg bg-red-600 p-6 text-white shadow-lg">
          <h1>Error: {data.error}</h1>
        </div>
      ) : (
        <>
          <h1 className="mb-2 border-b pb-2 text-center text-4xl tracking-tighter">
            {data.name}
          </h1>
          <div className="space-y-1 p-4">
            <p className="font-semibold">{data.desc}</p>
            <p>Only for privlidged: {data.restricted.toString()}</p>
            <p>Needs mod: {data.mod.toString()}</p>
          </div>
        </>
      )}
    </div>
  );
}

import { Link } from "react-router-dom";

export default function Header() {
  return (
    <header className="flex items-center border-b p-4 shadow">
      <h1 className="flex-grow text-2xl tracking-tighter text-gray-700">
        twitch bot
      </h1>
      <nav className="space-x-4">
        {["commands", "/"].map((item) => (
          <Link
            className="text-sky-400 hover:text-sky-600 hover:underline"
            to={`${item}`}
          >
            {item}
          </Link>
        ))}
      </nav>
    </header>
  );
}

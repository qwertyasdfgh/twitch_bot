import { NavLink, Outlet, useLoaderData } from "react-router-dom";

export async function loader() {
  return await fetch(`/api/v1/commands`).then((res) => res.json());
}

export default function Commands() {
  const data = useLoaderData();

  return (
    <div className="mx-auto max-w-4xl p-6">
      <ul className="flex flex-wrap items-center justify-center">
        {data.map((cmd, index) => (
          <li key={index}>
            <NavLink
              className={({ isActive }) =>
                isActive
                  ? "m-2 block rounded-xl border bg-gray-100 py-2 px-4 shadow hover:bg-gray-100"
                  : "m-2 block rounded-xl border py-2 px-4 shadow hover:bg-gray-100"
              }
              to={`${cmd.name}`}
            >
              <span>{cmd.name}</span>
            </NavLink>
          </li>
        ))}
      </ul>
      <Outlet />
    </div>
  );
}

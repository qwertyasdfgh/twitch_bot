import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import App, { Intro } from "./App";
import { Command, loader as commandLoader } from "./Command";
import Commands, { loader as commandsLoader } from "./Commands";
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <Intro />,
      },
      {
        path: "commands",
        element: <Commands />,
        loader: commandsLoader,
        children: [
          {
            path: ":commandName",
            loader: commandLoader,
            element: <Command />,
          },
        ],
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

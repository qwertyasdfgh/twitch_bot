import { Link, Outlet } from "react-router-dom";
import Header from "./Header";

export default function App() {
  return (
    <>
      <Header />
      <Outlet />
    </>
  );
}

export function Intro() {
  return (
    <div className="m-4">
      <h1>hello</h1>
      <p>this is a bot for twitch.tv</p>
    </div>
  );
}
